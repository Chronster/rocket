#pragma once
#include "stdafx.h"

class Collider {
public:
	Collider(){};
	Collider(clan::Vec2f upperLeft, clan::Vec2f upperRight, clan::Vec2f lowerLeft, clan::Vec2f lowerRight);
	//~Particle();

	void render(clan::Canvas canvas);
	bool testCollision(Particle &particle, FrictionGenerator &frictionGenerator);
	clan::Vec2f m_ul, m_ur, m_ll, m_lr, m_llul, m_ulur, m_urlr, m_lrll;		//vertices and edges
	clan::Vec2f m_collisionNormal1, m_collisionNormal2, m_collisionNormal3, m_collisionNormal4;

	float m_restitution;
	float m_mue;

private:
	float sign(const clan::Vec2f &p1, const clan::Vec2f &p2, const clan::Vec2f &p3);
	bool pointInTriangle(clan::Vec2f pt, clan::Vec2f v1, clan::Vec2f v2, clan::Vec2f v3);
	void calculateCollisionNormal(const clan::Vec2f &edge, clan::Vec2f &out);
	clan::Vec2f findCollisionNormal(const Particle &particle, clan::Vec2f &edge, clan::Vec2f &edgeStart);
	float cross2D(const clan::Vec2f &a, const clan::Vec2f &b);
	//clan::Vec2f m_collisionNormal1, m_collisionNormal2, m_collisionNormal3, m_collisionNormal4;
};