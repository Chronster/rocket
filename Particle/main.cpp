#include "stdafx.h"

using namespace clan;

// Choose the target renderer
//#define USE_SOFTWARE_RENDERER
//#define USE_OPENGL_1
#define USE_OPENGL_2

#ifdef USE_SOFTWARE_RENDERER
#include <ClanLib/swrender.h>
#endif

#ifdef USE_OPENGL_1
#include <ClanLib/gl1.h>
#endif

#ifdef USE_OPENGL_2
#include <ClanLib/gl.h>
#endif

int main(const std::vector<std::string> &args)
{
	const int WIDTH = 1024;
	const int HEIGHT = 768;

	SetupCore setup_core;
	SetupDisplay setup_display;
	SetupGL setup_gl;

	DisplayWindow window("PSO", WIDTH, HEIGHT);
	Canvas canvas(window);
	InputDevice keyboard = window.get_ic().get_keyboard();
	InputDevice mouse = window.get_ic().get_mouse();

	Particle particle = Particle(clan::Vec2f(10.0f, HEIGHT-10.0f), 7.0f, 1.0f, clan::Vec2f(0.0f, 0.0f), true);
	//particle.force += clan::Vec2f(20.0f, -25000000.0f);
	std::vector<Particle*> particleList;
	std::vector<Particle*> tmpParticleList;
	std::vector<Particle*> tmpDeletedParticleList;

	bool rocketStarted = false;
	bool rocketLives = true;

	std::vector<Collider*> obstacleList;
	Collider obstacle1 = Collider(clan::Vec2f(600.0f, 400.0f), clan::Vec2f(610.0f, 410.0f), clan::Vec2f(410.0f, 450.0f), clan::Vec2f(420.0f, 460.0f)); 
	Collider obstacle2 = Collider(clan::Vec2f(200.0f, 510.0f), clan::Vec2f(210.0f, 500.0f), clan::Vec2f(510.0f, 710.0f), clan::Vec2f(520.0f, 700.0f));
	obstacleList.push_back(&obstacle1);
	obstacleList.push_back(&obstacle2);

	Integrator integrator = Integrator();
	GravityGenerator gravityGen = GravityGenerator();
	FrictionGenerator frictionGen = FrictionGenerator();
	DragGenerator dragGen = DragGenerator();

	particleList.push_back(&particle);
	gravityGen.registerParticle(&particle);
	frictionGen.registerParticle(&particle);
	dragGen.registerParticle(&particle);
	

	/*std::vector<Obstacle> obstacles;
	obstacles.push_back(Obstacle(clan::Point(WIDTH/2-10, HEIGHT/2-20), clan::Point(WIDTH/2+10, HEIGHT/2+20)));
	obstacles.push_back(Obstacle(clan::Point(WIDTH/4-20, HEIGHT/4-10), clan::Point(WIDTH/4+20, HEIGHT/4+10)));
	Swarm *swarm = new Swarm(30, 4, WIDTH, HEIGHT, mouse.get_position(), &obstacles);*/
	unsigned int last_time = System::get_time();

	while (!keyboard.get_keycode(keycode_escape))
	{
		unsigned int current_time = System::get_time();
		float deltaTime = static_cast<float> (current_time - last_time);
		last_time = current_time;
		// Draw with the canvas:
		canvas.clear(Colorf::black);
		
		gravityGen.applyForce();
		dragGen.applyForce();

		for (int i=0; i<obstacleList.size(); i++) {
			for (int j=0; j<particleList.size(); j++) {
				obstacleList[i]->testCollision(*particleList[j], frictionGen);
			}
		}
		
		//gravityGen.applyForce();
		
		
		integrator.integrate(particleList, deltaTime/1000.0f);

		if (rocketStarted == false) {
			particle.force += clan::Vec2f(1233.0f, -2333.0f);
			rocketStarted = true;
		}

		if (particle.lifeTime <= 0.0f && rocketLives == true) {
			gravityGen.unregisterParticle(&particle);
			frictionGen.unregisterParticle(&particle);
			dragGen.unregisterParticle(&particle);
			particleList.erase(particleList.begin()+0);
			rocketLives = false;

			Particle particleFirstWave1 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(0.0f, -50.0f), true);
			Particle particleFirstWave2 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(-35.0f, -35.0f), true);
			Particle particleFirstWave3 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(-50.0f, 0.0f), true);
			Particle particleFirstWave4 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(-35.0f, 35.0f), true);
			Particle particleFirstWave5 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(0.0f, 50.0f), true);
			Particle particleFirstWave6 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(35.0f, 35.0f), true);
			Particle particleFirstWave7 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(50.0f, 0.0f), true);
			Particle particleFirstWave8 = Particle(clan::Vec2f(particle.position), 2.0f, 1.0f, clan::Vec2f(35.0f, -35.0f), true);

			particleList.push_back(&particleFirstWave1);
			particleList.push_back(&particleFirstWave2);
			particleList.push_back(&particleFirstWave3);
			particleList.push_back(&particleFirstWave4);
			particleList.push_back(&particleFirstWave5);
			particleList.push_back(&particleFirstWave6);
			particleList.push_back(&particleFirstWave7);
			particleList.push_back(&particleFirstWave8);

			gravityGen.registerParticle(&particleFirstWave1);
			gravityGen.registerParticle(&particleFirstWave2);
			gravityGen.registerParticle(&particleFirstWave3);
			gravityGen.registerParticle(&particleFirstWave4);
			gravityGen.registerParticle(&particleFirstWave5);
			gravityGen.registerParticle(&particleFirstWave6);
			gravityGen.registerParticle(&particleFirstWave7);
			gravityGen.registerParticle(&particleFirstWave8);

			frictionGen.registerParticle(&particleFirstWave1);
			frictionGen.registerParticle(&particleFirstWave2);
			frictionGen.registerParticle(&particleFirstWave3);
			frictionGen.registerParticle(&particleFirstWave4);
			frictionGen.registerParticle(&particleFirstWave5);
			frictionGen.registerParticle(&particleFirstWave6);
			frictionGen.registerParticle(&particleFirstWave7);
			frictionGen.registerParticle(&particleFirstWave8);

			dragGen.registerParticle(&particleFirstWave1);
			dragGen.registerParticle(&particleFirstWave2);
			dragGen.registerParticle(&particleFirstWave3);
			dragGen.registerParticle(&particleFirstWave4);
			dragGen.registerParticle(&particleFirstWave5);
			dragGen.registerParticle(&particleFirstWave6);
			dragGen.registerParticle(&particleFirstWave7);
			dragGen.registerParticle(&particleFirstWave8);
		}

		for (int i=0; i<particleList.size(); i++) {
			if (particleList[i]->lifeTime <= 0.0f && particleList[i]->isEmitter) {			
				Particle* particleSecondWave1 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(0.0f, -25.0f), false);
				Particle* particleSecondWave2 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(-17.5f, -17.5f), false);
				Particle* particleSecondWave3 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(-12.5f, 0.0f), false);
				Particle* particleSecondWave4 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(-17.5f, 17.5f), false);
				Particle* particleSecondWave5 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(0.0f, 25.0f), false);
				Particle* particleSecondWave6 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(17.5f, 17.5f), false);
				Particle* particleSecondWave7 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(25.0f, 0.0f), false);
				Particle* particleSecondWave8 = new Particle(clan::Vec2f(particleList[i]->position), 100.0f, 1.0f, clan::Vec2f(17.5f, -17.5f), false);

				tmpParticleList.push_back(particleSecondWave1);
				tmpParticleList.push_back(particleSecondWave2);
				tmpParticleList.push_back(particleSecondWave3);
				tmpParticleList.push_back(particleSecondWave3);
				tmpParticleList.push_back(particleSecondWave4);
				tmpParticleList.push_back(particleSecondWave5);
				tmpParticleList.push_back(particleSecondWave6);
				tmpParticleList.push_back(particleSecondWave7);
				tmpParticleList.push_back(particleSecondWave8);

				gravityGen.registerParticle(particleSecondWave1);
				gravityGen.registerParticle(particleSecondWave2);
				gravityGen.registerParticle(particleSecondWave3);
				gravityGen.registerParticle(particleSecondWave4);
				gravityGen.registerParticle(particleSecondWave5);
				gravityGen.registerParticle(particleSecondWave6);
				gravityGen.registerParticle(particleSecondWave7);
				gravityGen.registerParticle(particleSecondWave8);

				frictionGen.registerParticle(particleSecondWave1);
				frictionGen.registerParticle(particleSecondWave2);
				frictionGen.registerParticle(particleSecondWave3);
				frictionGen.registerParticle(particleSecondWave4);
				frictionGen.registerParticle(particleSecondWave5);
				frictionGen.registerParticle(particleSecondWave6);
				frictionGen.registerParticle(particleSecondWave7);
				frictionGen.registerParticle(particleSecondWave8);

				dragGen.registerParticle(particleSecondWave1);
				dragGen.registerParticle(particleSecondWave2);
				dragGen.registerParticle(particleSecondWave3);
				dragGen.registerParticle(particleSecondWave4);
				dragGen.registerParticle(particleSecondWave5);
				dragGen.registerParticle(particleSecondWave6);
				dragGen.registerParticle(particleSecondWave7);
				dragGen.registerParticle(particleSecondWave8);

				gravityGen.unregisterParticle(particleList[i]);
				frictionGen.unregisterParticle(particleList[i]);
				dragGen.unregisterParticle(particleList[i]);
				tmpDeletedParticleList.push_back(particleList[i]);
			
			} else if (particleList[i]->lifeTime <= 0.0f && !particleList[i]->isEmitter) {

				gravityGen.unregisterParticle(particleList[i]);
				frictionGen.unregisterParticle(particleList[i]);
				dragGen.unregisterParticle(particleList[i]);
				tmpDeletedParticleList.push_back(particleList[i]);
			}

			
			for(int i=0; i<tmpDeletedParticleList.size(); i++) {
				for(int j=0; j<particleList.size(); j++) {
					if (particleList[j] == tmpDeletedParticleList[i]) {
						particleList.erase(particleList.begin()+j);
					}
				}
			}

			for(int i=0; i<tmpParticleList.size(); i++) {
				particleList.push_back(tmpParticleList[i]);
			}

			tmpDeletedParticleList.clear();
			tmpParticleList.clear();

		}

		for (int i=0; i<obstacleList.size(); i++) {
			obstacleList[i]->render(canvas);
		}

		for (int i=0; i<particleList.size(); i++) {
			particleList[i]->render(canvas);
		}

		// Draw any remaining queued-up drawing commands on canvas:
		canvas.flush();
				
		// Present the frame buffer content to the user:
		window.flip();
 
		// Read messages from the windowing system message queue, if any are available:
		KeepAlive::process();
	}

	return 0;
}


// Create global application object:
// You MUST include this line or the application start-up will fail to locate your application object.
Application app(&main);
