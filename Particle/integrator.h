#pragma once
#include "stdafx.h"

class Integrator {
public:
	Integrator();
	
	void integrate(std::vector<Particle*> particles, float deltaTime);

private:
	unsigned int last_time;
};

